#include <stdlib.h>
#include <windows.h>
#include "helloscreen.h"
#include "tinycrt.h"

unsigned int FrameBufferAddr;

#ifdef EXTEST
void pattern() {
    for (int loop = 12; loop > 0; loop--) {
        unsigned short* framebuffer = (unsigned short*)FrameBufferAddr;//0x5D300000UL;
        for (int i = 0; i < 0xA8C00 / 2; i++) {
            *framebuffer = (unsigned short)((i + (loop << 2)) & 0xFFFF);
            framebuffer++;
#ifndef ARM
            // wait?
            //SleepEx(0, TRUE);
#endif
        }
    }
    return;
}

void skyblue() {
    fillscreen(RGB565(0, 0x72, 0xBA));
}

#endif

void fillscreen(unsigned short color) {
    // TODO: asm speed
    Wmemset((void*)FrameBufferAddr, color, 720 * 480);
}

#ifndef PORTRAIT
// landscape codes
void box(int x, int y, int w, int h, bool fill, bool fillfg) {
    if (w == 0 || h == 0) {
        return;
    }
    unsigned short* framebuffer = (unsigned short*)FrameBufferAddr;
    unsigned short* lefttopcorner = framebuffer + x + y * 720;
    if (fill) {
        for (int i = 0; i < h + 1; i++) {
            Wmemset(lefttopcorner + 1, (fillfg?0xFFFF:0), w - 1);
            lefttopcorner += 720;
        }
    } else {
        Wmemset(lefttopcorner, 0xFFFF, w + 1);
        Wmemset(lefttopcorner + h * 720, 0xFFFF, w + 1);
        for (int i = x + (y + 1) * 720; i < x + (y + h) * 720; i+=720) {
            *(framebuffer + i) = 0xFFFF;
            *(framebuffer + i + w) = 0xFFFF;
        }
    }
}

void dualbox(int x, int y, int w, int h, bool fl) {
    box(x, y, w, h, true, false); // Fill Cleanup
    box(x, y , w, h);
    if (fl) {
        box(x, y, w, 9);
        box(x + 2, y + 11, w - 4, h - 13);
    }
}
#endif // PORTRAIT

#ifndef GRAYFONT
#ifdef GUIWINB
unsigned int MyFont[100] = {
    8796552, 2308290, 1030272, 146400, 1,
    100873112, 330, 352137162, 150156228, 589435185,
    748333380, 69830, 272765064, 71438466, 10378,
    139459716, 210120704, 1047552, 138412032, 36909840,
    490399278, 138547396, 1042424366, 487854367, 277816609,
    488127551, 488160318, 138551871, 488159790, 521094702,
    6297600, 71499968, 272697480, 32506848, 71573634,
    134357550, 1008719614, 589284676, 521715247, 487621694,
    521717295, 1041284159, 34651199, 488015406, 588840497,
    474091662, 488129040, 580029609, 1041269793, 588830577,
    597350001, 488162862, 35112495, 748340782, 580372015,
    487856686, 138547359, 488162865, 145278513, 358274609,
    581046609, 138547537, 1042419999, 1013127390, 562436193,
    516305295, 324, 1040187392, 2180, 10834432,
    6640401, 6888960, 10835524, 101929472, 2240292,
    245146624, 5591825, 2236930, 625230852, 9811217,
    4858418, 727361536, 13980160, 6919680, 288707072,
    1281708544, 2255360, 7889376, 4861730, 11883776,
    6919424, 358269952, 13774080, 287459840, 15879936,
    813830424, 138412164, 102895683, 8860, 491913216
};
#elif GUIWINA
unsigned int MyFont[69] = {
    8796552, 2308290, 1030272, 146400, 1,
    100873112, 330, 352137162, 150156228, 589435185,
    748333380, 69830, 272765064, 71438466, 10378,
    139459716, 210120704, 1047552, 138412032, 36909840,
    490399278, 138547396, 1042424366, 487854367, 277816609,
    488127551, 488160318, 138551871, 488159790, 521094702,
    6297600, 71499968, 272697480, 32506848, 71573634,
    134357550, 1008719614, 589284676, 521715247, 487621694,
    521717295, 1041284159, 34651199, 488015406, 588840497,
    474091662, 488129040, 580029609, 1041269793, 588830577,
    597350001, 488162862, 35112495, 748340782, 580372015,
    487856686, 138547359, 488162865, 145278513, 358274609,
    581046609, 138547537, 1042419999, 1013127390, 562436193,
    516305295, 324, 1040187392, 2180
};
#endif

void writetext(int x, int y, char* str, unsigned short fgcolor) {
    unsigned short* framebuffer = (unsigned short*)FrameBufferAddr;
    unsigned short* lefttopcorner = framebuffer + x + y * 720;
    //9100 FOR D=1 TO LEN(CO$):HN=FT(ASC(MID$(CO$,D,1))-28)
    //9110 IF HN=1 THEN XN=XN+2:9140
    //9120 FOR Y=0 TO 5: FOR X=0 TO 4:IF HN<>INT(HN/2)*2 THEN DRAW XN+X,YN+Y
    //9130 HN=INT(HN/2):NEXT X:NEXT Y:XN=XN+6
    //9140 NEXT D:RETURN
    while (*str != 0) {
        int hc = *str - 28;
        str++;
        if (hc < 0 || hc >= _countof(MyFont)) {
            continue;
        }
        if (hc == 4) {
            // space? tab?
            lefttopcorner += 2;
            continue;
        }
        int hn = MyFont[hc];
        if (hn == 1) {
            //space? tab?
            lefttopcorner += 2;
            continue;
        }
#ifdef GUIWINB
        if (hc > 68 && hc<95 && hc!=81 && hc!=91){
            for (int iy = 0; iy < 8; iy++) {
                for (int ix = 0; ix < 4; ix++) {
                    if (hn % 2 == 1) {
                        *(lefttopcorner + ix + iy * 720) = fgcolor;
                    }
                    hn = hn / 2;
                }
            }
            lefttopcorner += 5;
        } else {
            for (int iy = 0; iy < 6; iy++) {
                for (int ix = 0; ix < 5; ix++) {
                    if (hn % 2 == 1) {
                        *(lefttopcorner + ix + iy * 720) = fgcolor;
                    }
                    hn = hn / 2;
                }
            }
            lefttopcorner += 6;
        }
#else
        for (int iy = 0; iy < 6; iy++) {
            for (int ix = 0; ix < 5; ix++) {
                if (hn % 2 == 1) {
                    *(lefttopcorner + ix + iy * 720) = fgcolor;
                }
                hn = hn / 2;
            }
        }
        lefttopcorner += 6;
#endif
    }
}
#endif // GRAYFONT

#ifdef GRAYFONT
#include "Candara.h"
#endif //GRAYFONT

#ifdef PORTRAIT

unsigned short* getcorner(int x, int y) {
    unsigned short* framebuffer = (unsigned short*)FrameBufferAddr;
    unsigned short* lefttopcorner = framebuffer + y + (479 - x) * 720;
    return lefttopcorner;
}

// use Qt rules
// 012
// 1..
// 2..
// = w2, h2. max w = 479 max h = 719
void FillRect(int x, int y, int w, int h, unsigned short color) {
    unsigned short* lefttopcorner = getcorner(x, y);
    for (int i = 0; i < w + 1; i++) {
        Wmemset(lefttopcorner, color, h + 1);
        lefttopcorner -= 720;
    }
}

// same as FillRect.
// border: 1 = +1, 2 = -1,+1, 3 = -1, +1, +2, 4 = -2, -1, +1, +2
// corner skip for rounded corner
void FrameRect(int x, int y, int w, int h, unsigned short color, int border, unsigned char cornerskip) {
    unsigned short* lefttopcorner = getcorner(x, y);
    if (border == 1) {
        Wmemset(lefttopcorner, color, h + 1);
        for (int i = 0; i < w - 1; i++) {
            lefttopcorner -= 720;
            *(lefttopcorner) = color;
            *(lefttopcorner + h) = color;
        }
        Wmemset(lefttopcorner - 720, color, h + 1);
    } else {
        // style1: use FillRect 4 times
        // style2: faster Wmemset optimize
        // b-, b+, lr--, t-, t+
        lefttopcorner += (border / 2) * 719 + cornerskip; // 2 = -1, 3 = -1, 4 = -2
        for (int i = 0; i < border; i++) {
            Wmemset(lefttopcorner, color, h + 1 - cornerskip * 2);
            lefttopcorner -= 720;
        }
        if (cornerskip > 0) {
            // jump 8pix
            lefttopcorner -= 721 * cornerskip - border * 720;
        }
        for (int i = 0; i < w - border - 1 - (cornerskip > 0 ? (cornerskip - border) * 2:0); i++) {
            Wmemset(lefttopcorner, color, border);
            lefttopcorner += (h - border + 1);
            Wmemset(lefttopcorner, color, border);
            lefttopcorner -= (h - border + 1 + 720);
        }
        if (cornerskip > 0) {
            // jump 8pix
            lefttopcorner -= 719 * cornerskip - border * 720;
        }
        for (int i = 0; i < border; i++) {
            Wmemset(lefttopcorner, color, h + 1 - cornerskip * 2);
            lefttopcorner -= 720;
        }
    }
}

// border = 3, skip = 10
void RoundedRect(int x, int y, int w, int h, unsigned short color) {
    FrameRect(x, y, w, h, color, 3, 10);
    DrawExGraphic(x - 1, y - 1, 94, color, false, false); // TODO: EXGRAPHIC1
    DrawExGraphic(x + w - 9, y - 1, 94, color, true, false);
    DrawExGraphic(x + w - 9, y + h - 10, 94, color, true, true);
    DrawExGraphic(x - 1, y + h - 10, 94, color, false, true);
}
#endif // PORTRAIT

#ifdef GRAYFONT

unsigned char quickmix(unsigned char a, unsigned char b, unsigned char alpha) {
    //return (((a - b) * alpha) >> 8) + b;
    return (((a - b) * alpha) / 255) + b;
}

unsigned short quickmixrgb(unsigned short fgcolor, unsigned bgcolor, unsigned char alpha) {
    return quickmix(fgcolor & 0x1F, bgcolor & 0x1F, alpha) | (quickmix((fgcolor >> 5) & 0x3F, (bgcolor >> 5) & 0x3F, alpha) << 5) | (quickmix((fgcolor >> 11) & 0x7FF, (bgcolor >> 11) & 0x7FF, alpha) << 11);
}

void WriteGrayText(int x, int y, char* str, unsigned short fgcolor) {
    unsigned short* lefttopcorner = getcorner(x, y);
    while (*str != 0) {
        int index = (unsigned char)*str - '!'; // TODO: start char
        str++;
        if (index < 0) {
            lefttopcorner -= 6 * 720; // Space?
            continue;
        }
        if (index >= GRAPHICCOUNT) {
            // High
            continue;
        }
        int dpos = FastTable[index];
        if (dpos == 65535) {
            // NULL
            continue;
        }
        unsigned short ParaItem = ParaTable[index];
        int width   = WidthTable[ParaItem & ((1 << WIDTHBITS) - 1)]; // 1F
        int height  = HeightTable[(ParaItem >> WIDTHBITS) & ((1 << HEIGHTBITS) - 1)]; // F
        int basefix = BaseFix[(ParaItem >> (WIDTHBITS + HEIGHTBITS + LEFTBITS + RIGHTBITS)) & ((1 << BASEBITS) - 1)]; // F

        // align by baseline
        lefttopcorner += BASELINE - (height + basefix);

        int pos = 0;
        for (int ix = 0; ix < width; ix++) {
            for (int iy = 0; iy < height; iy++) {
                unsigned char dpix = GraphicData[dpos];
                unsigned char pix = 0;
                if (pos == 0) {
                    pix = dpix & 0xF;
                } else {
                    pix = dpix >> 4;
                }
                pix = AlphaTable[pix];
                //unsigned short mixedpix = ((fgcolor & 0x1F) * pix / 16) | ((((fgcolor >> 5) & 0x3F) * pix / 16) << 5) | ((((fgcolor >> 11) & 0x7FF) * pix / 16) << 11);
                unsigned short bgcolor = *(lefttopcorner + iy - ix * 720);
                *(lefttopcorner + iy - ix * 720) = quickmixrgb(fgcolor, bgcolor, pix);
                pos++;
                if (pos == 2) {
                    pos = 0;
                    dpos++;
                }
            }
        }
        lefttopcorner -= (width + 2) * 720;
        lefttopcorner -= BASELINE - (height + basefix); // restore from baseline
    }
}
void DrawExGraphic(int x, int y, int index, unsigned short fgcolor, bool hflip, bool vflip) {
    unsigned short* lefttopcorner = getcorner(x, y);
    int dpos = FastTable[index];
    unsigned short ParaItem = ParaTable[index];
    int width   = WidthTable[ParaItem & ((1 << WIDTHBITS) - 1)]; // 1F
    int height  = HeightTable[(ParaItem >> WIDTHBITS) & ((1 << HEIGHTBITS) - 1)]; // F

    int pos = 0;
    //for (int ix = 0; ix < width; ix++) {
    //    for (int iy = 0; iy < height; iy++) {
    for (int ix = (hflip?width-1:0); ix != (hflip?-1:width); ix+=(hflip?-1:1)) {
        for (int iy = (vflip?height-1:0); iy != (vflip?-1:height); iy+=(vflip?-1:1)) {
            unsigned char dpix = GraphicData[dpos];
            unsigned char pix = 0;
            if (pos == 0) {
                pix = dpix & 0xF;
            } else {
                pix = dpix >> 4;
            }
            pix = AlphaTable[pix];
            unsigned short bgcolor = *(lefttopcorner + iy - ix * 720);
            *(lefttopcorner + iy - ix * 720) = quickmixrgb(fgcolor, bgcolor, pix);
            pos++;
            if (pos == 2) {
                pos = 0;
                dpos++;
            }
        }
    }
}
#endif