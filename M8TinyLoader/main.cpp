#include <stdlib.h>
#include "HelloScreen.h"
#include "tinydebug.h"
#include "tinycrt.h"
#include "tinyoal.h"

#ifdef REGISTERPRINT
extern "C" extern unsigned int IPValue;
extern "C" extern unsigned int SPValue;
extern "C" extern unsigned int LRValue;
extern "C" extern unsigned int PCValue;
extern "C" extern unsigned int SBValue;
extern "C" extern unsigned int R0Value[16];
#endif //REGISTERPRINT

extern "C" extern void myLoadKernel();
extern "C" extern void myLoadXip();


void delayLoop1(int count)			// CYM
{
    volatile int j,i;
    for(j = 0; j < count; j++)
        for(i=0;i<1000;i++);
}

#ifndef PORTRAIT
void PrepareTinyLandscapeMenu();
void UpdateTinyLandscapeMenuSelection( bool android );
void UpdateTinyLandscapeTimeout( int percent );
#else
#define SCREENBG RGB565(0x3B, 0x1B, 0x09)
#define DARKTEXT RGB565(0xC7, 0xC7, 0xC7)
#define LIGHTTEXT RGB565(0xE7, 0xE7, 0xE7)
void PreparePortraitMenu();
void UpdatePortraitMenuSelection( bool android );
void UpdatePortraitTimeout( int percent );
#endif
#ifdef SCROLLANIME
void ScrollScreen(bool left, volatile UINT32& addr);
#endif

void start() {
    volatile S3C6410_DISPLAY_REG *pDispReg = (S3C6410_DISPLAY_REG *)OALPAtoVA(S3C6410_BASE_REG_PA_DISPLAY, FALSE);
    FrameBufferAddr = pDispReg->VIDW01ADD0B0; //0x5D300000UL;
#ifdef SCROLLANIME
    FrameBufferAddr += 480 * 720 * 2;
#endif
#ifdef PORTRAIT
    fillscreen(SCREENBG);
#else
    fillscreen(RGB565(0x19,0x64,0x7A));
#endif
    delayLoop1(3);

#ifdef REGISTERPRINT
    int xpos = 300;
    int ypos = 180;
    dualbox(xpos - 5, ypos - 18, 160, 200, true);
    writetext(xpos - 3, ypos - 16, "REGISTER ARRAY");
    //ypos += 17;

    char hex[17] = "IP: 0x"; // R15
    uint2hex(IPValue, 8, &hex[6]);
    writetext(xpos, ypos, hex);
    ypos += 9;
    delayLoop1(3);

    hex[0] = 'S';
    hex[1] = 'P';
    uint2hex(SPValue, 8, &hex[6]);
    writetext(xpos, ypos, hex);
    ypos += 9;
    delayLoop1(3);

    hex[0] = 'L';
    hex[1] = 'R';
    uint2hex(LRValue, 8, &hex[6]);
    writetext(xpos, ypos, hex);
    ypos += 9;
    delayLoop1(3);

    hex[0] = 'P';
    hex[1] = 'C';
    uint2hex(PCValue, 8, &hex[6]);
    writetext(xpos, ypos, hex);
    ypos += 9;
    delayLoop1(3);

    hex[0] = 'S';
    hex[1] = 'B';
    uint2hex(SBValue, 8, &hex[6]);
    writetext(xpos, ypos, hex);
    ypos += 9;
    delayLoop1(3);

    hex[0] = 'R';
    for (int i = 0; i < _countof(R0Value); i++) {
        // 9 = SB, 12 = IP, 13 = SP, 14 = LR, 15 = PC
        if (i == 9 || i >= 12) {
            continue;
        }
        if (i < 10) {
            hex[1] = '0' + i;
        } else {
            hex[1] = '0' + i / 10;
            hex[2] = '0' + i % 10;
            hex[3] = ':';
            hex[4] = ' ';
            hex[5] = '0';
            hex[6] = 'x';
        }
        uint2hex(R0Value[i], 8, &hex[i<10?6:7]);
        hex[16] = 0;
        writetext(xpos, ypos, hex);
        ypos += 9;
        delayLoop1(20);
    }
    char hint[] = "WAITING 1SEC...";
    for (int i = 0; i < 3; i++) {
        writetext(xpos, ypos, hint);
        delayLoop1(680);
        box(xpos - 1, ypos, 86, 7, true, false);
        delayLoop1(340);
    }
#endif // REGISTERPRINT
#ifndef GRAYFONT
    dualbox(5, 3, 120, 49, true);
    writetext(10, 5, "UART Check");
    writetext(10, 17, "SERIAL INIT...");
#endif
    OEMInitDebugSerial();
#ifndef GRAYFONT
    writetext(10, 26, "SERIAL CHECK...");
#endif
    OEMWriteDebugString("M8TinyLoader: Check UART2.\r\n");
#ifndef GRAYFONT
    OEMWriteDebugString("pDispReg->VIDW01ADD0B0:");
    printhex(pDispReg->VIDW01ADD0B0, 8);
    OEMWriteDebugString("\r\n");
#endif

#ifdef UARTREADTEST
    for (int i = 0; i < 120; i++) {
        char ch = OEMReadDebugByte();
        if (ch != OEM_DEBUG_READ_NODATA){
            OEMWriteDebugByte(ch);
        } else if (i % 10 == 0) {
            OEMWriteDebugByte('.');
        }
        delayLoop1(500);
    }
#endif
#ifndef GRAYFONT
    writetext(10, 35, "SERIAL OK.");
#endif

#ifdef DUALBOOT
    S3C6410_GPIO_REG* GPIOReg = (S3C6410_GPIO_REG *)OALPAtoVA(S3C6410_BASE_REG_PA_GPIO, FALSE);
    GPIOReg->GPNPUD |= 0x28UL;          // 00 10 10 00
    GPIOReg->GPNPUD &= 0xFFFFFFEBUL;    // 11 10 10 11

    // it's selector
    bool android = true, prvandroid = true;

#ifdef PORTRAIT
    // Draw a smooth portrait menu
    PreparePortraitMenu();
#else
    // Draw a tiny landscape menu
    PrepareTinyLandscapeMenu();
#endif
    for (int percent = 0; percent < 100; percent++) {
        if (android != prvandroid || percent == 0) {
            // android
#ifdef PORTRAIT
            UpdatePortraitMenuSelection(android);
#else
            UpdateTinyLandscapeMenuSelection(android);
#endif
            prvandroid = android;
        }
#ifdef PORTRAIT
        UpdatePortraitTimeout(percent);
#else
        UpdateTinyLandscapeTimeout(percent);
#endif
        // if percent = 0, we should prepare animate because all graphic done?
#ifdef SCROLLANIME
        if (percent == 0) {
            ScrollScreen(true, pDispReg->VIDW01ADD0B0);
        }
#endif
        char ch = OEMReadDebugByte();
        if (ch != OEM_DEBUG_READ_NODATA) {
            // TODO: force redraw on 100?
            if (ch == 'a' || ch == 'A') {
                android = true;
                OEMWriteDebugString("Selected Android.\r\n");
            }
            if (ch == 'c' || ch == 'C') {
                android = false;
                OEMWriteDebugString("Selected MMobile.\r\n");
            }
            if (prvandroid != android && percent == 99) {
                percent--;
                continue;
            }
        }
        unsigned short key = GPIOReg->GPNDAT;
        if ((key | 0x236) != key) { // 1000110110
            // some keys down
            // check volume?
            if ((key | 4) != key) {
                // volume + pushed
                android = true;
                OEMWriteDebugString("Selected Android.\r\n");
            }
            if ((key | 2) != key) {
                // volume - pushed
                android = false;
                OEMWriteDebugString("Selected MMobile.\r\n");
            }
            if ((key | 16) != key) {
                // cancel pushed
                android = !android;
                OEMWriteDebugString("Selection reversed.\r\n");
                delayLoop1(300);
            }
            if ((key | 512) != key) {
                // power button
                percent = 99;
                OEMWriteDebugString("Power up!\r\n");
#ifdef PORTRAIT
                UpdatePortraitTimeout(percent);
#else
                UpdateTinyLandscapeTimeout(percent);
#endif
                break; // prevent loop
            }
            if (prvandroid != android && percent == 99) {
                percent--;
                continue;
            }
        }
        delayLoop1(200);
    }

    // restore button
    GPIOReg->GPNPUD |= 0x14UL;          // 00 01 01 00
    GPIOReg->GPNPUD &= 0xFFFFFFD7UL;    // 11 01 01 11

#ifdef SCROLLANIME
    if (!android) {
        ScrollScreen(false, pDispReg->VIDW01ADD0B0);
    }
#endif
#endif // DUALBOOT

#ifdef DUALBOOT
    if (android) {
        myLoadKernel();
    } else {
        myLoadXip(); // TODO: restore stack?
    }
#else
    myLoadKernel();
#endif
}

#ifndef PORTRAIT

void PrepareTinyLandscapeMenu()
{
    dualbox(245, 120, 100, 100, false);
    dualbox(375, 120, 100, 100, false);
    writetext(245, 235, "Push LEFT for Android, RIGHT for MMobile.");
    // save me!
    const int left = 200, top = 300;
    box(left, top, 320, 80, true, false); // a black background
    box(left + 60, top + 20, 252, 12); // screen
    box(left + 60, top + 32, 252, 30); // back case
    box(left + 56, top + 31, 4, 12); // power button
    writetext(left + 17, top + 35, "POWER");
    box(left + 80, top + 31, 28, 12, true, false); // Left
    box(left + 80, top + 31, 28, 12);
    box(left + 113, top + 31, 28, 12, true, false); // right
    box(left + 113, top + 31, 28, 12);
    box(left + 108, top + 31, 5, 8, true, false);
    box(left + 108, top + 33, 5, 8);
    writetext(left + 82, top + 47, "LEFT");
    writetext(left + 113, top + 47, "RIGHT");
    box(left + 302, top, 18, 80, true, false); // terminator

    box(245, 260, 230, 20, false, true); // Progress bar frame
    box(246, 261, 228, 18, true, false);
}

void UpdateTinyLandscapeMenuSelection( bool android )
{
    box(247, 122, 96, 96, true, android);
    box(377, 122, 96, 96, true, !android);
    writetext(275, 170, "ANDROID", android?0:0xFFFF);
    writetext(405, 170, "MMOBILE", android?0xFFFF:0);
}

void UpdateTinyLandscapeTimeout( int percent )
{
    box(247, 262, (226*percent)/99, 16, true, true);
}

#else

void PreparePortraitMenu() {
    FillRect(0, 24, 92, 2, RGB565(0x82, 0x53, 0x3B));
    FillRect(99, 34, 2, 116, RGB565(0x82, 0x53, 0x3B));
    FillRect(0, 158, 92, 2, RGB565(0x82, 0x53, 0x3B));
    DrawExGraphic(92, 24, 94, RGB565(0x82, 0x53, 0x3B), true, false);
    DrawExGraphic(92, 151, 94, RGB565(0x82, 0x53, 0x3B), true, true);
    WriteGrayText(20, 46, "Up", DARKTEXT);
    WriteGrayText(20, 107, "Down", DARKTEXT);
    FillRect(304, 0, 2, 44, RGB565(0x82, 0x53, 0x3B));
    FillRect(314, 52, 138, 2, RGB565(0x82, 0x53, 0x3B));
    FillRect(460, 0, 2, 44, RGB565(0x82, 0x53, 0x3B));
    DrawExGraphic(304, 45, 94, RGB565(0x82, 0x53, 0x3B), false, true);
    DrawExGraphic(453, 45, 94, RGB565(0x82, 0x53, 0x3B), true, true);
    WriteGrayText(342, 13, "Confirm", DARKTEXT);
    WriteGrayText(144, 77, "Press volume up / down to", DARKTEXT);
    WriteGrayText(144, 107, "move selection, press power", DARKTEXT);
    WriteGrayText(144, 137, "to boot selected os.", DARKTEXT);
    WriteGrayText(91, 553, "Waiting 8 seconds for boot...", LIGHTTEXT);
    RoundedRect(68, 493, 348, 40, RGB565(0xF6, 0xCB, 0xBB)); // Progress
    WriteGrayText(202, 382, "DJYOS", RGB565(0x81, 0x50, 0x38));
    RoundedRect(131, 362, 212, 65, RGB565(0x7A, 0x48, 0x2F));
}

void UpdatePortraitMenuSelection( bool android ) {
    FillRect(130, 217, 213, 137, SCREENBG);
    FillRect(133, android ? 220 : 292, 207, 59, RGB565(0xBA, 0x58, 0x27));
    RoundedRect(131, android ? 218 : 290, 212, 65, RGB565(0xF6, 0xCB, 0xBB));
    RoundedRect(131, android ? 290 : 218, 212, 65, RGB565(0x7A, 0x48, 0x2F));
    WriteGrayText(197, 242, "Android", 0xFFFF);
    WriteGrayText(190, 314, "MMobile", LIGHTTEXT);
}

void UpdatePortraitTimeout( int percent ) {
    //percent = 5;
    if (percent < 6 || percent > 95) {
        //FillRect(67, 492, 9, 40, SCREENBG);
        FillRect(67, 492, 9, 9, SCREENBG);
        FillRect(67, 523, 9, 9, SCREENBG);
        //FillRect(407, 492, 9, 40, SCREENBG);
        FillRect(407, 492, 9, 9, SCREENBG);
        FillRect(407, 523, 9, 9, SCREENBG);
    }
    // TODO: Delta because we can't go back
    FillRect((percent<6||percent>95?70:80), 495, (343 * percent) / 99 - (percent<6||percent>95?0:10), 34, RGB565(0xDA, 0x81, 0x54));
    if (percent > 0 && percent < 99) {
        // clip right
        DrawExGraphic((343 * percent) / 99 + 64, 495, 95, SCREENBG, true, false);
        DrawExGraphic((343 * percent) / 99 + 64, 523, 95, SCREENBG, true, true);
    }
    if (percent < 6 || percent > 95) {
        RoundedRect(68, 493, 348, 40, RGB565(0xF6, 0xCB, 0xBB));
    }
}

#endif // GRAYFONT

#ifdef SCROLLANIME

void ScrollScreen( bool left, volatile UINT32& addr )
{
    /*for (int i = 0; i < 480; i++) {
        addr += left?1440:-1440;
        //delayLoop1(1);
    }*/
    addr += left?720*480*2:720*480*-2;
}

#endif // SCROLLANIME