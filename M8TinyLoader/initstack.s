REGISTERPRINT   EQU 0
ATAGVERIFY      EQU 0
DUALBOOT        EQU 1

    AREA .text, CODE, READONLY
		EXPORT myStackStartup

myStackStartup  PROC
    [ REGISTERPRINT
    STR IP, IPValue
    STR SP, SPValue
    STR LR, LRValue
    STR PC, PCValue
    STR SB, SBValue
    STR R0, R0Value
    STR R1, R0Value + 4
    STR R2, R0Value + 8
    STR R3, R0Value + 12
    STR R4, R0Value + 16
    STR R5, R0Value + 20
    STR R6, R0Value + 24
    STR R7, R0Value + 28
    STR R8, R0Value + 32
    ;STR R9, R0Value + 36 ; SB
    STR R10, R0Value + 40
    STR R11, R0Value + 44
    ;STR R12, R0Value + 48 ; IP
    STR R13, R0Value + 52
    STR R14, R0Value + 56
    STR R15, R0Value + 60
    ]
    LDR R0, =StackAddr
    LDR SP, [R0]
    B |?start@@YAXXZ|

        IMPORT |?start@@YAXXZ|

StackAddr
    DCD 0x50100C00

myStackStartup ENDP

    [ REGISTERPRINT
        EXPORT IPValue
        EXPORT SPValue
        EXPORT LRValue
        EXPORT PCValue
        EXPORT SBValue
        EXPORT R0Value ; Array
IPValue
    DCD 0
SPValue
    DCD 0
LRValue
    DCD 0
PCValue
    DCD 0
SBValue
    DCD 0
R0Value ; R0~R15
    ;DCD 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 31
    SPACE 64
    ]
 

    AREA .text, CODE, READONLY
        EXPORT myLoadKernel

myLoadKernel   PROC
    ; try search kernel overlay first in normal mode
    ; ArmMagic will overwrite to KernelAddress by LiteXipBuiler in dualboot mode
    [ DUALBOOT
    LDR R3, ArmZMagic
    |
    LDR R0, ArmZMagic
    MOV R3, PC
    MOV R2, #0xFF000000
    ORR R2, R2, #0x00FF0000
    ORR R2, R2, #0x0000F000
    AND R3, R3, R2 ; 1000 increcment at 24
    ORR R3, R3, #0x24
labelcmp
    LDR R1, [R3]
    CMP R1, R0
    BEQ labelboot
    ADD R3, R3, #0x1000
    B labelcmp
labelboot
    AND R3, R3, R2 ; -24, get jump target in R3 now
    ]

    ; OK?
    STMFD   SP!, {R3}
    MOV R0, R3
    ORR R0, R0, #0x24 ; +24
    LDR R1, =8
    LDR R2, =FoundHintHex
    ;ADD R2, R2, 26
    BL uint2hex

    LDR R0, =FoundHint
    BL OEMWriteDebugString
    [ ATAGVERIFY
    LDR R0, =0x50000100
    LDR R0, [R0]
    LDR R1, =8
    LDR R2, =FoundHintHex
    BL uint2hex
    LDR R0, =FoundHintHex
    BL OEMWriteDebugString
    ]

    ; armmemcpy r0 = dest r1 = src, r2 = size
    LDR R0, =0x50000100 ; aesop kernel read fixed offset atag
    LDR R1, =Param
    LDR R2, =8*4
    BL optmemcpy

    [ ATAGVERIFY
    LDR R0, =0x50000100
    LDR R0, [R0]
    LDR R1, =8
    LDR R2, =FoundHintHex
    BL uint2hex
    LDR R0, =FoundHintHex
    BL OEMWriteDebugString
    LDR R0, =0x50000104
    LDR R0, [R0]
    LDR R1, =8
    LDR R2, =FoundHintHex
    BL uint2hex
    LDR R0, =FoundHintHex
    BL OEMWriteDebugString
    ]

    LDMFD   SP!, {R3}

    MOV R0, #0
    MOV R1, #0x650
    ORR R1, R1, #0xA ; 65A = 1626
    LDR R2, =Param ; TODO: copy to 0x50000100

    MOV PC, R3 ; jump without link

        IMPORT OEMWriteDebugString
        IMPORT optmemcpy
        IMPORT uint2hex

ArmZMagic
    DCD 0x016F2818 ; zImageMagic
Param
    DCD 2
    DCD 0x54410001  ; CORE
    DCD 4
    DCD 0x54410002  ; MEM
    DCD 1024*1024*256; 256M
    DCD 0x50000000  ; From 80M offset
    ;DCD 4
    ;DCD 0x54410002  ; MEM
    ;DCD 0x00C00000  ; 16M
    ;DCD 0x5D300000  ; FrameBuffer
    ;DCD 4
    ;DCD 0x54410002  ; MEM
    ;DCD 0x02800000  ; 48M
    ;DCD 0x50100000  ; NK
    ;DCD 0x02700000  ; 48M
    ;DCD 0x50200000  ; NK
    DCD 0
    DCD 0x00000000  ;NONE
FoundHint
    DCB "Found zImage magic at 0x"
FoundHintHex
    DCB "00000000.\r\n", 0
    align 4

myLoadKernel ENDP

    [ DUALBOOT
    AREA .text, CODE, READONLY
        EXPORT myLoadXip

myLoadXip PROC
    LDR R0, RestoreHolder
    LDR R1, =0x50100000
    STR R0, [R1]
    DCB "J.WO"
RestoreHolder
    DCB "NG" ; quick n dirty than ArmZMagic
    align 4
myLoadXip ENDP

    ]


    [ 0
    AREA .text, CODE, READONLY
        EXPORT armmemcpy
;
; Called from C as int armmemcpy(void*, const void*, int);
; The first 4 parameters are passed in r0-r3, more parameters would be passed on the stack
;
armmemcpy PROC
	MOVS    R2, R2,LSL #31
	LDRCSH  R3, [R1],#2
	LDRMIB  R2, [R1],#1
	STRCSH  R3, [R0],#2
	STRMIB  R2, [R0],#1
	BX      LR
armmemcpy ENDP
    ]
    END
